﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ImportScript_WorkSchedule
{
    public static class ImportHelper
    {
        public static string OpenFileLocation()
        {
            System.Windows.Forms.OpenFileDialog openFileDialog = new System.Windows.Forms.OpenFileDialog();
            //openFileDialog.ShowDialog();
            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                return openFileDialog.FileName;
            else 
            return string.Empty;
        }
    }
}
