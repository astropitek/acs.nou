﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using DocsVision.BackOffice.CardLib.CardDefs;

using DocsVision.Platform.CardHost;
using DocsVision.Platform.Extensibility;
using DocsVision.Platform.ObjectManager;
using DocsVision.Platform.WinForms;

namespace ACS.NOU.ScheduleExtension
{
    /// <summary>
    /// Представляет расширение Навигатора библиотеки карточек "Расписания занятий"
    /// </summary>
    /// <remarks>
    /// Предоставляет кнопки (команды) ленты Навигатора для начала работы с расписаниями
    /// </remarks>
    [ComVisible(true), ClassInterface(ClassInterfaceType.None), Guid("106CAB08-48FA-43F0-AF70-9AB039D9DD47")]
    public class ScheduleExtension : NavExtension
    {
        /// <summary>
        /// Возвращает значение перечисления <see cref="T:DocsVision.Platform.Extensibility.NavExtensionTypes"/>,
        /// определяющее типы расширений, поддерживаемые данным расширением
        /// </summary>
        protected override NavExtensionTypes SupportedTypes
        {
            get { return NavExtensionTypes.Command; }
        }
        /// <summary>
        /// Инициализирует новый экземпляр класса
        /// </summary>
        public ScheduleExtension() : base() { }
        // <summary>
        /// Возвращает название расширения 
        /// </summary>
        /// <param name="extensionType">Перечисление <see cref="T:DocsVision.Platform.Extensibility.NavExtensionTypes"/>,
        /// представляющее типы расширения</param>
        /// <returns>Объект <see cref="T:System.String"/>, представляющий название расширения</returns>
        protected override string GetExtensionName(NavExtensionTypes extensionType)
        {
            return ACS.NOU.ScheduleExtension.Resources.ExtensionName;
        }
        /// <summary>
        /// Выполняет создание коллекции команд, предоставляемых данным расширением
        /// </summary>
        /// <returns>Коллекция, содержащая команды расширения</returns>
        protected override IEnumerable<NavCommand> CreateCommands()
        {
            return new NavCommand[]
            {
                CreateScheduleCommand()
            };
        }
        /// <summary>
        /// Выполняет определение текущего статуса указанной команды в зависимости от контекста
        /// </summary>
        /// <param name="command">Команда, для которой выполняется запрос статуса</param>
        /// <param name="context">Текущий контекст команд</param>
        /// <returns>Флаги перечисления <see cref="T:DocsVision.Platform.Extensibility.NavCommandStatus"/>,
        /// представляющие текущий статус указанной команды</returns>
        protected override NavCommandStatus QueryCommandStatus(NavCommand command, NavCommandContext context)
        {
            return NavCommandStatus.Supported | NavCommandStatus.Enabled;
        }
        /// <summary>
        /// Выполняет вызов указанной команды, предоставляемой текущим расширением
        /// </summary>
        /// <param name="command">Команда, для которой запрашивается выполнение</param>
        /// <param name="context">Текущий контекст команд</param>
        protected override void InvokeCommand(NavCommand command, NavCommandContext context)
        {
            if (string.Equals(command.Name, ACS.NOU.ScheduleExtension.Resources.CreateScheduleCommandName))
            {
                InvokeCreateScheduleCommand();
            }
            else base.InvokeCommand(command, context);
        }
        private NavCommand CreateScheduleCommand()
        {
            return new NavCommand
            {
                CommandType = NavCommandTypes.ToolBar | NavCommandTypes.Picture,
                Name = ACS.NOU.ScheduleExtension.Resources.CreateScheduleCommandName,
                Description = ACS.NOU.ScheduleExtension.Resources.CreateScheduleCommandName,
                Icon = ACS.NOU.ScheduleExtension.Resources.CreateScheduleIcon,
                Location = new NavCommandLocation
                {
                    Menu = MenuLocation.Grid,
                    RibbonTabLocations = new RibbonTabLocation[]
                    {
                        new RibbonTabLocation
                        {
                            Tab = ACS.NOU.ScheduleExtension.Resources.CommandLocationTabName,
                            Groups = new string[]
                            {
                                ACS.NOU.ScheduleExtension.Resources.CommandLocationGroupName
                            }
                        }
                    }
                }
            };
        }
        /// <summary>
        /// Выполняет действия, представляемые командой ""
        /// </summary>
        private void InvokeCreateScheduleCommand()
        {
            MessageBox.Show("В разработке.");
            /* Guid folderID = FoldersData.GetGuid("RequestsFolder").GetValueOrDefault();
             Guid kindID = KindsData.GetGuid("RequestKind").GetValueOrDefault();
             Guid stateID = GetFirstStateByKind(kindID);

             CreateDocument(kindID, stateID, folderID);*/
        }
    }
}
