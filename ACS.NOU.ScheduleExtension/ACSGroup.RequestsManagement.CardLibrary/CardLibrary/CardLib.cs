﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;

using DocsVision.Platform.WinForms;

namespace ACSGroup.RequestsManagement.CardLibrary
{
    /// <summary>
    /// Представляет компонент библиотеки карточек "Управление заявками и инцидентами"
    /// </summary>
    [ComVisible(true), ClassInterface(ClassInterfaceType.None), Guid("0A623D35-70A7-47C5-B6DB-082A6AA2BBA3")]
    public class CardLib : DocsVision.Platform.WinForms.CardLibrary
    {
        #region Конструкторы
        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="T:ACSGroup.RequestsManagement.CardLibrary.CardLib"/>
        /// </summary>
        public CardLib() : base() { }
        #endregion

        #region Методы, переопределяющие методы класса CardLibrary
        /// <summary>
        /// Возвращает версию библиотеки карточек "Управление заявками и инцидентами"
        /// </summary>
        /// <returns>Объект класса <see cref="T:System.Version"/>, представляющий версию библиотеки карточек</returns>
        public override Version GetVersion()
        {
            return new Version(5, 3, 2559);
        }
        /// <summary>
        /// Возвращает иконку библиотеки карточек "Управление заявками и инцидентами"
        /// </summary>
        /// <returns>Объект класса <see cref="T:System.Drawing.Icon"/>, представляющий иконку библиотеки карточек</returns>
        public override Icon GetIcon()
        {
            return Properties.Resources.LibraryIcon;
        }
        /// <summary>
        /// Возвращает иконку карточки по идентификатору её типа
        /// </summary>
        /// <param name="cardTypeId">Идентификатор типа карточки</param>
        /// <returns>Объект класса <see cref="T:System.Drawing.Icon"/>, представляющий иконку карточки</returns>
        public override Icon GetCardIcon(Guid cardTypeId)
        {
            if (cardTypeId == Cards.RefRequests)
            {
                return Properties.Resources.LibraryIcon;
            }
            else
            {
                return base.GetCardIcon(cardTypeId);
            }
        }
        /// <summary>
        /// Возвращает строку с описанием схемы данных карточки по идентификатору её типа
        /// </summary>
        /// <param name="cardTypeId">Идентификатор типа карточки</param>
        /// <returns>Строка, содержащая схему данных карточки</returns>
        public override string GetCardDefinition(Guid cardTypeId)
        {
            if (cardTypeId == Cards.RefRequests)
            {
                return Properties.Resources.RefRequestsDefinition;
            }
            else if (cardTypeId == Cards.Extension)
            {
                return Properties.Resources.RequestsExtensionDefinition;
            }
            else
            {
                return base.GetCardDefinition(cardTypeId);
            }
        }
        #endregion
    }
}