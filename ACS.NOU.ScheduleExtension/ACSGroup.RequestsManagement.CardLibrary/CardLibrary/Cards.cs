﻿using System;

namespace ACSGroup.RequestsManagement
{
    /// <summary>
    /// Представляет класс, содержащий идентификаторы типов карточек библиотеки "Управление заявками и инцидентами"
    /// </summary>
    internal static class Cards
    {
        #region Константы - идентификаторы типов карточек библиотеки
        /// <summary>
        /// Идентификатор типа карточки "Справочник заявок и инцидентов"
        /// </summary>
        public static readonly Guid RefRequests = new Guid("DF3EB3BE-47AA-4C40-AF53-A4468434FB52");
        /// <summary>
        /// Идентификатор типа карточки "Создание заявок и инцидентов" (расширение Навигатора)
        /// </summary>
        public static readonly Guid Extension = new Guid("4B59A789-E31B-4263-B44F-CF2E061ACDA8");
        #endregion
    }
}
