﻿using System;

namespace ACSGroup.RequestsManagement.Extension
{
    /// <summary>
    /// Представляет класс, описывающий структуру данных карточки "Справочник заявок и инцидентов"
    /// </summary>
    public static class RefRequests
    {
        /// <summary>
        /// Идентификатор типа карточки
        /// </summary>
        public static readonly Guid ID = new Guid("DF3EB3BE-47AA-4C40-AF53-A4468434FB52");
        /// <summary>
        /// Псевдоним типа карточки
        /// </summary>
        public const string Alias = "RefRequestsFolders";
        /// <summary>
        /// Представляет класс, описывающий структуру секции "Папки для создания карточек" карточки "Справочник заявок и инцидентов"
        /// </summary>
        public static class Folders
        {
            /// <summary>
            /// Псевдоним секции
            /// </summary>
            public const string Alias = "Folders";
            /// <summary>
            /// Псевдоним поля "Папка для создания служебных записок"
            /// </summary>
            public const string ServiceNotesFolder = "ServiceNotesFolder";
            /// <summary>
            /// Псевдоним поля "Папка для создания согласований служебных записок"
            /// </summary>
            public const string ServiceNotesReconciliationsFolder = "ServiceNotesReconciliationsFolder";
            /// <summary>
            /// Псевдоним поля "Папка для создания инцидентов"
            /// </summary>
            public const string RequestsFolder = "RequestsFolder";
            /// <summary>
            /// Идентификатор секции
            /// </summary>
            public static readonly Guid ID = new Guid("4EA03265-8141-469A-9170-B78124743DB9");
        }
        /// <summary>
        /// Представляет класс, описывающий структуру секции "Виды карточек" карточки "Справочник заявок и инцидентов"
        /// </summary>
        public static class Kinds
        {
            /// <summary>
            /// Псевдоним секции
            /// </summary>
            public const string Alias = "Kinds";
            /// <summary>
            /// Псевдоним поля "Вид карточки "Инцидент""
            /// </summary>
            public const string RequestKind = "RequestKind";
            /// <summary>
            /// Псевдоним поля "Вид карточки "Служебная записка""
            /// </summary>
            public const string ServiceNoteKind = "ServiceNoteKind";
            /// <summary>
            /// Идентификатор секции
            /// </summary>
            public static readonly Guid ID = new Guid("A6E704F9-202D-4610-81CD-0F9AFDED51AD");
        }
    }
}