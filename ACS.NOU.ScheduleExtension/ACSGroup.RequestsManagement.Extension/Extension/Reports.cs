﻿using System;

namespace ACSGroup.RequestsManagement.Extension
{
    /// <summary>
    /// Представляет статический класс, содержащий идентификаторы отчётов библиотеки карточек "Управление заявками и инцидентами"
    /// </summary>
    public static class Reports
    {
        /// <summary>
        /// Представляет идентификатор отчёта "Получить начальное состояние по виду карточки"
        /// </summary>
        public static readonly Guid GetFirstStateByKind = new Guid("719C8C8B-A515-4564-BB41-5256404F6F44");
    }
}