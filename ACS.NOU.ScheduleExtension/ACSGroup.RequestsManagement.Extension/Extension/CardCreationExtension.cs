﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using DocsVision.BackOffice.CardLib.CardDefs;

using DocsVision.Platform.CardHost;
using DocsVision.Platform.Extensibility;
using DocsVision.Platform.ObjectManager;
using DocsVision.Platform.WinForms;

namespace ACSGroup.RequestsManagement.Extension
{
    /// <summary>
    /// Представляет расширение Навигатора библиотеки карточек "Управление заявками и инцидентами"
    /// </summary>
    /// <remarks>
    /// Предоставляет кнопки (команды) ленты Навигатора для создания карточек заявок и инцидентов
    /// </remarks>
    [ComVisible(true), ClassInterface(ClassInterfaceType.None), Guid("106CAB08-48FA-43F0-AF70-9AB039D9DD47")]
    public class CardCreationExtension : NavExtension
    {
        #region Поля и свойства
        /// <summary>
        /// Представляет объект данных карточки "Справочник заявок и инцидентов"
        /// </summary>
        private CardData DictionaryData;
        /// <summary>
        /// Представляет объект данных строки секции "Виды карточек" карточки "Справочник заявок и инцидентов"
        /// </summary>
        private RowData KindsData;
        /// <summary>
        /// Представляет объект данных строки секции "Папки для создания карточек" карточки "Справочник заявок и инцидентов"
        /// </summary>
        private RowData FoldersData;
        /// <summary>
        /// Возвращает значение перечисления <see cref="T:DocsVision.Platform.Extensibility.NavExtensionTypes"/>,
        /// определяющее типы расширений, поддерживаемые данным расширением
        /// </summary>
        protected override NavExtensionTypes SupportedTypes
        {
            get { return NavExtensionTypes.Command; }
        }
        #endregion

        #region Конструкторы
        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="T:ACSGroup.RequestsManagement.Extension.CardCreationExtension"/>
        /// </summary>
        public CardCreationExtension() : base() { }
        #endregion

        #region Методы класса
        /// <summary>
        /// Выполняет создание события <see cref="E:DocsVision.Platform.WinForms.CardControl.CardInitialized"/>
        /// </summary>
        /// <param name="args">Аргументы, передаваемые событию</param>
        protected override void OnCardInitialized(EventArgs args)
        {
            base.OnCardInitialized(args);

            DictionaryData = Session.CardManager.GetDictionaryData(RefRequests.ID);
            KindsData = DictionaryData.Sections[RefRequests.Kinds.ID].FirstRow;
            FoldersData = DictionaryData.Sections[RefRequests.Folders.ID].FirstRow;
        }
        /// <summary>
        /// Возвращает название расширения "Создание заявок и инцидентов"
        /// </summary>
        /// <param name="extensionType">Перечисление <see cref="T:DocsVision.Platform.Extensibility.NavExtensionTypes"/>,
        /// представляющее типы расширения</param>
        /// <returns>Объект <see cref="T:System.String"/>, представляющий название расширения</returns>
        protected override string GetExtensionName(NavExtensionTypes extensionType)
        {
            return Properties.Resources.ExtensionName;
        }
        /// <summary>
        /// Выполняет создание коллекции команд, предоставляемых данным расширением
        /// </summary>
        /// <returns>Коллекция, содержащая команды расширения</returns>
        protected override IEnumerable<NavCommand> CreateCommands()
        {
            return new NavCommand[]
            {
                CreateRequestCreationCommand(),
                CreateServiceNoteCreationCommand()
            };
        }
        /// <summary>
        /// Выполняет определение текущего статуса указанной команды в зависимости от контекста
        /// </summary>
        /// <param name="command">Команда, для которой выполняется запрос статуса</param>
        /// <param name="context">Текущий контекст команд</param>
        /// <returns>Флаги перечисления <see cref="T:DocsVision.Platform.Extensibility.NavCommandStatus"/>,
        /// представляющие текущий статус указанной команды</returns>
        protected override NavCommandStatus QueryCommandStatus(NavCommand command, NavCommandContext context)
        {
            return NavCommandStatus.Supported | NavCommandStatus.Enabled;
        }
        /// <summary>
        /// Выполняет вызов указанной команды, предоставляемой текущим расширением
        /// </summary>
        /// <param name="command">Команда, для которой запрашивается выполнение</param>
        /// <param name="context">Текущий контекст команд</param>
        protected override void InvokeCommand(NavCommand command, NavCommandContext context)
        {
            if (string.Equals(command.Name, Properties.Resources.CreateRequestCommandName))
            {
                InvokeRequestCreationCommand();
            }
            else if (string.Equals(command.Name, Properties.Resources.CreateServiceNoteCommandName))
            {
                InvokeServiceNoteCreationCommand();
            }
            else base.InvokeCommand(command, context);
        }
        #endregion

        #region Вспомогательные методы расширения
        /// <summary>
        /// Выполняет создание команды "Создание инцидента"
        /// </summary>
        /// <returns>Инициализированный экземпляр команды</returns>
        private NavCommand CreateRequestCreationCommand()
        {
            return new NavCommand
            {
                CommandType = NavCommandTypes.ToolBar | NavCommandTypes.Picture,
                Name = Properties.Resources.CreateRequestCommandName,
                Description = Properties.Resources.CreateRequestCommandDescription,
                Icon = Properties.Resources.CreateRequestIcon,
                Location = new NavCommandLocation
                {
                    Menu = MenuLocation.Grid,
                    RibbonTabLocations = new RibbonTabLocation[]
                    {
                        new RibbonTabLocation
                        {
                            Tab = Properties.Resources.CommandLocationTabName,
                            Groups = new string[]
                            {
                                Properties.Resources.CommandLocationGroupName
                            }
                        }
                    }
                }
            };
        }
        /// <summary>
        /// Выполняет создание команды "Создание служебной записки"
        /// </summary>
        /// <returns>Инициализированный экземпляр команды</returns>
        private NavCommand CreateServiceNoteCreationCommand()
        {
            return new NavCommand
            {
                CommandType = NavCommandTypes.ToolBar | NavCommandTypes.Picture,
                Name = Properties.Resources.CreateServiceNoteCommandName,
                Description = Properties.Resources.CreateServiceNoteCommandDescription,
                Icon = Properties.Resources.CreateServiceNoteIcon,
                Location = new NavCommandLocation
                {
                    Menu = MenuLocation.Grid,
                    RibbonTabLocations = new RibbonTabLocation[]
                    {
                        new RibbonTabLocation 
                        {
                            Tab = Properties.Resources.CommandLocationTabName,
                            Groups = new string[]
                            {
                                Properties.Resources.CommandLocationGroupName
                            }
                        }
                    }
                }
            };
        }
        /// <summary>
        /// Выполняет действия, представляемые командой "Создать инцидент"
        /// </summary>
        private void InvokeRequestCreationCommand()
        {
            Guid folderID = FoldersData.GetGuid("RequestsFolder").GetValueOrDefault();
            Guid kindID = KindsData.GetGuid("RequestKind").GetValueOrDefault();
            Guid stateID = GetFirstStateByKind(kindID);

            CreateDocument(kindID, stateID, folderID);
        }
        /// <summary>
        /// Выполняет действия, представляемые командой "Создать служебную записку"
        /// </summary>
        private void InvokeServiceNoteCreationCommand()
        {
            Guid folderID = FoldersData.GetGuid("ServiceNotesFolder").GetValueOrDefault();
            Guid kindID = KindsData.GetGuid("ServiceNoteKind").GetValueOrDefault();
            Guid stateID = GetFirstStateByKind(kindID);

            CreateDocument(kindID, stateID, folderID);
        }
        /// <summary>
        /// Выполняет создание карточки документа указанного вида и открывает созданную карточку в указанной папке
        /// </summary>
        /// <param name="kindID">Идентификатор вида карточки</param>
        /// <param name="stateID">Идентификатор состояния карточки</param>
        /// <param name="folderID">Идентификатор папки для создания ярлыка карточки</param>
        private void CreateDocument(Guid kindID, Guid stateID, Guid folderID)
        {
            CardData documentData = Session.CardManager.CreateCardData(CardDocument.ID);
            documentData.BeginUpdate();
            RowData documentSystemData = documentData.Sections[CardDocument.System.ID].FirstRow;
            documentSystemData.SetGuid("Kind", kindID);
            documentSystemData.SetGuid("State", stateID);
            documentData.EndUpdate();

            CardFrame.CardHost.ShowCard(documentData.Id, Guid.Empty, ActivateMode.Edit, ActivateFlags.New, null, folderID);
        }
        /// <summary>
        /// Выполняет получение начального состояния карточки по идентификатору её вида
        /// </summary>
        /// <param name="kindID">Идентификатор вида карточки</param>
        /// <returns>Структура <see cref="T:System.Guid"/>, представляющая идентификатор состояния карточки</returns>
        private Guid GetFirstStateByKind(Guid kindID)
        {
            if (Session.ReportManager.Reports.Contains(Reports.GetFirstStateByKind))
            {
                Report stateReport = Session.ReportManager.Reports[Reports.GetFirstStateByKind];
                stateReport.Parameters["KindID"].Value = kindID;
                InfoRowCollection results = stateReport.GetData();
                if (results.Count > 0)
                {
                    return results[0].GetGuid("StateID").GetValueOrDefault();
                }
            }
            return Guid.Empty;
        }
        #endregion
    }
}