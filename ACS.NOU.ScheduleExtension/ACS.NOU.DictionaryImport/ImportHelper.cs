﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using DocsVision.BackOffice.ObjectModel;
using DocsVision.BackOffice.WinForms.Design.LayoutItems;
using DocsVision.Platform.ObjectModel;
using DocsVision.Platform.ObjectManager;

using Microsoft.Office.Interop.Excel;

namespace ACS.NOU.DictionaryImport
{
    public class ImportHelper
    {
        #region Properties
        private ObjectContext Context;
        private UserSession Session;
        private Guid CardID { get; set; }
        private ExcelHelper ExcelHelper { get; set; }
        #endregion

        #region Constructor
        public ImportHelper(UserSession session, ObjectContext context)
        {
            Context = context;
            Session = session;
        }
        #endregion

        #region Methods
        public void ImportFile(string filePath, BaseCard card,ITableControl table)
        {
            //MessageBox.Show("Start");
           
            ExcelHelper = new ExcelHelper(filePath);
            int i = 0;
            int startBlockIndex = ExcelHelper.Worksheet.UsedRange.Columns.Count;
            //int startSubBlockIndex = ExcelHelper.Worksheet.UsedRange.Columns.Count;
            bool start = false;
            IList<Block> bloks = new List<Block>();
            Block lastBlock = null;
            //SubBlock lastSubBlock = null;
            //MessageBox.Show("Step 1");
            int lastRowIndex = ExcelHelper.SearchString("ИТОГО:").Rows.Row;//ExcelHelper.FindStringInColumn(2, "ИТОГО:");
            int lastColumnIndex = ExcelHelper.SearchString("ИТОГО").Columns.Column;//ExcelHelper.FindStringInRow(12, "ИТОГО");
            MessageBox.Show(lastRowIndex.ToString());
            MessageBox.Show(lastColumnIndex.ToString());
            while (i < lastRowIndex/*ExcelHelper.Worksheet.UsedRange.Columns.Count*/)
            {
                
                i++;
                if (ExcelHelper.GetCellValue(i, 1) == "1.1.1")
                {
                    start = true;
                    startBlockIndex = i;
                }
                //if (ExcelHelper.GetCellValue(i, 1) != string.Empty)
                //{ }
                if (!start)
                    continue;
                //MessageBox.Show("Step 1.1");
                if (startBlockIndex == i)
                {
                    //MessageBox.Show(ExcelHelper.GetCellValue(i, 1));
                    lastBlock = new Block();
                    lastBlock.StartRow = i;
                    lastBlock.Code = ExcelHelper.GetCellValue(i, 1);
                    lastBlock.Name = ExcelHelper.GetCellValue(i, 2);
                    lastBlock.Description = ExcelHelper.GetCellValue(i+1, 2);
                    bloks.Add(lastBlock);
                }
                /*
                if(i> startBlockIndex && i == startSubBlockIndex)
                {
                    lastSubBlock = new SubBlock();
                    lastSubBlock.StartRow = i;
                    lastSubBlock.StudentsCount = Convert.ToInt32(ExcelHelper.GetCellValue(i, 53));
                    lastBlock.SubBlocks.Add(lastSubBlock);
                }*/
                
                if (ExcelHelper.GetCellValue(i+1, 1) != string.Empty)
                {
                    //MessageBox.Show("Step 1.2");
                    startBlockIndex = i + 1;
                    if(lastBlock != null)
                        lastBlock.EndRow = i;
                }
                /*
                if (ExcelHelper.GetCellValue(i + 1, 53) != string.Empty )
                {
                    //MessageBox.Show("Step 1.2");
                    if (startBlockIndex != i + 1)
                        startSubBlockIndex = i + 1;
                    if (lastSubBlock != null)
                        lastSubBlock.EndRow = i;
                }*/


            }
           // MessageBox.Show("Step 2");
            //добавление ряда в таблицу справочника
            while (table.RowCount > 0)
                 table.RemoveRow(card, table.RowCount - 1);
            BaseCardProperty row = null;
            foreach (Block block in bloks)
            {
                /* foreach (SubBlock subBlock in block.SubBlocks)
                 {*/

                int i1 = block.StartRow + 1;//subBlock.StartRow;
                    while (i1 <= block.EndRow/*subBlock.EndRow*/)
                    {
                       // MessageBox.Show(i1.ToString() + " " + ExcelHelper.GetCellValue(i1, 3));
                        row = table.AddRow(card);
                        row["Code"] = block.Code;
                        row["Name"] = block.Name;
                        row["Description"] = ExcelHelper.FindFirstNotNullUpp(i1, 2);
                    try{
                        //MessageBox.Show(ExcelHelper.FindFirstNotNullUpp(i1, 53));
                        row["StudentsCount"] = Convert.ToInt32(ExcelHelper.FindFirstNotNullUpp(i1, lastColumnIndex));
                    } catch { }//subBlock.StudentsCount;
                    row["Form"] = ExcelHelper.GetCellValue(i1, 3);
                    try { row["StartDate"] = Convert.ToDateTime(ExcelHelper.GetCellValue(i1, 4)); } catch { }
                    try { row["EndDate"] = Convert.ToDateTime(ExcelHelper.GetCellValue(i1, 5)); } catch { }
                    /*if(ExcelHelper.GetCellValue(i1, 6) != string.Empty)
                        row["HoursCount"] = Convert.ToInt32(ExcelHelper.GetCellValue(i1, 6));*/
                    string hours = ExcelHelper.FindFirstNotNullUpp(i1,6);
                        //MessageBox.Show(hours);
                        if (hours != string.Empty)
                            try { row["HoursCount"] = Convert.ToInt32(hours); } catch { }
                            
                        table.RefreshRow(table.RowCount - 1);
                        //if ()
                        i1++;
                    } 
               // }
            }

            ExcelHelper.Application.Quit();
        }

        #endregion
    }
}
