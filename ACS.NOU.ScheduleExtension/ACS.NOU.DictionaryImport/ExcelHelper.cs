﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Office.Interop.Excel;


namespace ACS.NOU.DictionaryImport
{
    public class ExcelHelper
    {
        #region Properties
        public Application Application;
        public Workbook Workbook;
        public Worksheet Worksheet;
        #endregion
        //Microsoft.Office.Interop.Excel.Application oExcel = new Microsoft.Office.Interop.Excel.Application();
        //Microsoft.Office.Interop.Excel.Workbook oWb = oExcel.Workbooks.Open(DocumentService.DownloadMainFile(templateCard, templateCard.Files.Where(df => df.FileType == DocumentFileType.Main).FirstOrDefault()));
        //Microsoft.Office.Interop.Excel.Worksheet oWs = oWb.Worksheets[1] as Microsoft.Office.Interop.Excel.Worksheet;
        public ExcelHelper(string filePath)
        {
            Application = new Application();
            Workbook = Application.Workbooks.Open(filePath);
            Worksheet = Workbook.Worksheets[1];
        }

        public string GetCellValue(int rowIndex, int columnIndex)
        {
            string cellValue = string.Empty;

            Range cellRange = (Range)Worksheet.Cells[rowIndex, columnIndex];
            if (cellRange.Value != null)
            {
                cellValue = cellRange.Value.ToString();
            }
            return cellValue;
        }
        public string FindFirstNotNullUpp(int row, int column)
        {
            //string result = null;
            int i = row;
            while (i > 0 && this.GetCellValue(i, column) == string.Empty)
            {
                i--;
            }
            return this.GetCellValue(i, column);
        }
        public int FindStringInColumn(int column, string toFind)
        {
            int result = 0;
            int i = 1;
            while (i <= Worksheet.UsedRange.Column)
            {
                if (this.GetCellValue(i, column) == toFind)
                    result = i;
                i++;
            }
            return result;
        }
        public int FindStringInRow(int row, string toFind)
        {
            int result = 0;
            int i = 1;
            while (i <= Worksheet.UsedRange.Row)
            {
                if (this.GetCellValue(row, i) == toFind)
                    result = i;
                i++;
            }
            return result;
        }
        public Range SearchString(string findText)
        {
            return Worksheet.Cells.Find(findText, Type.Missing,
            XlFindLookIn.xlValues, XlLookAt.xlPart,
            XlSearchOrder.xlByRows, XlSearchDirection.xlNext, false,
            Type.Missing, Type.Missing);
        }
    }
}
