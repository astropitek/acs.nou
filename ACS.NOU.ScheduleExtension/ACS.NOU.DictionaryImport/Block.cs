﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACS.NOU.DictionaryImport
{
    internal class Block
    {
        public int StartRow { get; set; }
        public int EndRow { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public IList<SubBlock> SubBlocks;

        public Block()
        {
            SubBlocks = new List<SubBlock>();
        }
        //int IList
    }
}
