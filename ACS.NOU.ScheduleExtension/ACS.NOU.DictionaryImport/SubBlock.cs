﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACS.NOU.DictionaryImport
{
    internal class SubBlock
    {
        public int StartRow { get; set; }
        public int EndRow { get; set; }
        public int StudentsCount { get; set; }
    }
}
