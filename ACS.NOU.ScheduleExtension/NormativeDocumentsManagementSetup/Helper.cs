﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Xml.Linq;
using System.Xml.XPath;
using DocsVision.BackOffice.ObjectModel;
using DocsVision.BackOffice.ObjectModel.Services;
using DocsVision.Platform.CardHost;
using DocsVision.Platform.Cards.Constants;
using DocsVision.Platform.ObjectManager;
using DocsVision.Platform.ObjectManager.SystemCards;
using DocsVision.Platform.ObjectModel;
using DocsVision.Platform.ObjectModel.Search;
using ObjectState = DocsVision.Platform.ObjectManager.ObjectState;

namespace ACSGroup.DvHelper
{
    public class Helper : IDisposable
    {

        protected DvConnection Connection { get; }

        //public List<string> Errors;
        public DvConnectionSettings Settings => Connection.Settings;
        public UserSession Session => Connection.Session;
        public ObjectContext Context => Connection.Context;
        protected ICardHost CardHost => Connection.CardHost;
        public IDocumentService DocumentService => Context.GetService<IDocumentService>();
        public IStaffService StaffService => Context.GetService<IStaffService>();
        protected IBaseCardService BaseCardService => Context.GetService<IBaseCardService>();
        protected IBaseUniversalService BaseUniversalService => Context.GetService<IBaseUniversalService>();
        protected IVersionedFileCardService VersionedFileCardService => Context.GetService<IVersionedFileCardService>();
        public IStateService StateService => Context.GetService<IStateService>();
        protected ITaskListService TaskListService => Context.GetService<ITaskListService>();
        public IReferenceListService ReferenceListService => Context.GetService<IReferenceListService>();
        protected ICategoriesService CategoriesService => Context.GetService<ICategoriesService>();
        protected ICategoryListService CategoryListService => Context.GetService<ICategoryListService>();
        protected ILinkService LinkService => Context.GetService<ILinkService>();
        protected IKindService KindService => Context.GetService<IKindService>();
        public IPartnersService PartnerService => Context.GetService<IPartnersService>();



        public Helper(DvConnection connection)
        {
            if (!connection.IsSessionAlive) connection.Connect();
            if (!connection.IsSessionAlive) throw new ArgumentException("Connection is closed");
            Connection = connection;
        }
        public Helper(string serverName, string baseName, string userName, string password) : this(new DvConnection(new DvConnectionSettings(serverName,baseName,userName,password)))
        {            
        }

//        public Helper() : this(new DvConnection(new DvConnectionSettings()))
//        {            
//        }

        public bool IsConnected => Connection.IsSessionAlive;

        protected void CreateReference(Guid first, Guid second, string linkTypeName, bool createBackRef = true)
        {
            var linkType = LinkService.FindLink(linkTypeName);
            if (linkType == null) throw new ArgumentException($"Тип ссылки {linkTypeName} не найден ");
            ReferenceListService.CreateReference(GetReferenceList(first), linkType, second, GetTypeId(second), false);
            if (!createBackRef) return;
            var oppositeLink = LinkService.FindOppositeLink(linkType);
            //Context.SaveObject(Context.GetObject)
            if (oppositeLink != null)
                ReferenceListService.CreateReference(GetReferenceList(second), oppositeLink, first, GetTypeId(first), false);
        }

        protected ReferenceList GetReferenceList(Guid docId)
        {
            var card = Session.CardManager.GetCardData(docId);
            if (card == null) throw new ArgumentException($"Документ {docId} не найден");
            var main = card.Sections[card.Type.Sections["MainInfo"].Id].FirstRow;
            if (main.GetGuid("ReferenceList") == Guid.Empty)
                main.SetGuid("ReferenceList", Context.SaveObject(ReferenceListService.CreateReferenceList()).Id);
            return Context.GetObject<ReferenceList>(main.GetGuid("ReferenceList"));
        }

        public void CreateShortcut(Guid doc, Guid folder, bool isStrong)
        {
            if (!FolderExist(folder)) throw new ArgumentException($"Folder not exist {folder}");
            var folderCard = ((FolderCard)Session.CardManager.GetDictionary(FoldersCard.ID));
            folderCard?.CreateShortcut(folder, doc, isStrong);
        }

        public void DeleteShortcut(Guid id)
        {
            var folderCard = ((FolderCard)Session.CardManager.GetDictionary(FoldersCard.ID));
            folderCard.DeleteShortcut(id, false);
        }
        protected Guid GetTypeId(Guid docId)
        {
            return Session.CardManager.GetCardData(docId).Type.Id;
        }
        public Guid FindFolder(string name, Guid root)
        {
            var folder = ((FolderCard)Session.CardManager.GetDictionary(FoldersCard.ID)).GetFolder(root);
            if (string.Equals(folder.Name, name, StringComparison.CurrentCultureIgnoreCase))
                return folder.Id;
            foreach (var f in folder.Folders)
            {
                var found = FindFolder(name, f.Id);
                if (found != Guid.Empty)
                    return found;
            }
            return Guid.Empty;
        }
        public bool FolderExist(Guid id)
        {
            return ((FolderCard)Session.CardManager.GetDictionary(FoldersCard.ID)).FolderExists(id);
        }
        
        public IEnumerable<Guid> FolderDocs(Guid folderId)
        {
            if (!FolderExist(folderId)) return new List<Guid>();
            var folder = ((FolderCard)Session.CardManager.GetDictionary(FoldersCard.ID)).GetFolder(folderId);
            return folder.Shortcuts.Where(x => !x.Deleted && !x.CardId.Equals(Guid.Empty)).Select(x => x.CardId);
        }
        public IEnumerable<Guid> FolderShortCuts(Guid folderId)
        {
            if (!FolderExist(folderId)) return new List<Guid>();
            var folder = ((FolderCard)Session.CardManager.GetDictionary(FoldersCard.ID)).GetFolder(folderId);
            var docs = folder.Shortcuts.Where(x => !x.Deleted && !x.CardId.Equals(Guid.Empty)).Select(x => x.Id);
            return docs;
        }
        public RowData GetSectionData(CardData card, string name) => card.Sections[card.Type.Sections[name].Id].FirstRow;
        public RowDataCollection GetSectionsRows(CardData card, string name) => card.Sections[card.Type.Sections[name].Id].Rows;
        public CardData GetCardData(Guid id)
        {
            if (Session.CardManager.GetCardState(id) != ObjectState.Existing)
                throw new ArgumentException($"Карточка {id} не существует или удалена");
            return Session.CardManager.GetCardData(id);
        }
        public XElement GetCardXml(Guid id)
        {
            if (Session.CardManager.GetCardState(id) != ObjectState.Existing)
                throw new ArgumentException($"Карточка {id} не существует или удалена");
            using (var ms = new MemoryStream())
            {
                Session.CardManager.GetCardData(id)?.SaveXml(ms);
                ms.Position = 0;
                return XDocument.Load(ms).Root;
            }
        }
        public IEnumerable<T> CardXPath<T>(XElement root, string xPath)
        {
            return root == null ? new T[0] : ((IEnumerable)root.XPathEvaluate(xPath)).Cast<T>();
        }

        public IEnumerable<T> CardXpath<T>(Guid id, string xpath)
        {
            var root = GetCardXml(id);
            return root == null ? new T[0] : CardXPath<T>(root, xpath);
        }

        public Guid GetDepartmentId(string name)
        {
            if (string.IsNullOrEmpty(name)) return Guid.Empty;
            var department = Context.FindObject<StaffUnit>(new QueryObject("Name", name));
            if (department == null) return Guid.Empty;
            var obj = Context.GetObjectRef(department);
            return obj?.Id ?? Guid.Empty;
        }

        public Guid GetUniversalItem(string parent, string name, bool createIfNoExist)
        {
            var type = BaseUniversalService.FindItemTypeWithSameName(parent, null);
            if (type == null) return Guid.Empty;
            var item = BaseUniversalService.FindItemWithSameName(name, type);
            if (item != null) return Context.GetObjectRef(item).Id;
            if (!createIfNoExist) return Guid.Empty;
            var newItem = BaseUniversalService.AddNewItem(type);
            newItem.Name = name;
            return Context.SaveObject(newItem).Id;
        }

        public void Disconnect()
        {
            if (IsConnected)
                Connection.Close();
        }

        public void DeleteDoc(Guid guid)
        {
            Session.CardManager.DeleteCard(guid);
        }
        public Guid CreateDocument(string name)
        {
            var doc = DocumentService.CreateDocument();
            doc.MainInfo.Name = name;
            doc.Description = name;
            return Context.SaveObject(doc).Id;
        }

        public string CreateDocument(string kindId, string folderId, string subject, string[] files)
        {
            var kind = Context.GetObject<KindsCardKind>(new Guid(kindId));
            var document = DocumentService.CreateDocument(files[0], kind);
            Context.SaveObject(document);

            DocumentService.CreateListCards(document);
            var currentEmployee = StaffService.GetCurrentEmployee();
            document.MainInfo.Name = subject;
            document.MainInfo.Author = currentEmployee;
            document.MainInfo.Registrar = currentEmployee;
            Context.SaveObject(document);
            var id = Context.GetObjectRef(document).Id;
            try
            {
                document.Description = BaseCardService.GenerateDigest(document, Session.CardManager.GetCardData(id, true), subject);
            }
            catch (Exception)
            {
                document.Description = document.MainInfo.Name;
            }

            if (files.Length > 1)
            {
                DocumentService.AddMainFiles(document, files.Skip(1));
            }
            Context.SaveObject(document);
            if (!string.IsNullOrEmpty(folderId))
            {
                var folderCard = (FolderCard)Session.CardManager.GetDictionary(FoldersCard.ID);
                folderCard.CreateShortcut(new Guid(folderId), id, false);
            }
            return id.ToString();
        }

        public Document GetDocument(Guid id)
        {
            var state = Session.CardManager.GetCardState(id);
            return  state == ObjectState.Existing ? Context.GetObject<Document>(id) : null;
        }
        public void ShowCard(string id)
        {
            CardHost.ShowCardModal(new Guid(id), ActivateMode.Edit);
        }

        public void SelectCard(Guid g)
        {
            CardHost.SelectCard("Тест");
        }
        public void DeleteFilesAndDir(string path)
        {
            var fi = new FileInfo(path);
            if (fi.DirectoryName == null)
            {
                fi.Delete();
                return;
            }
            var di = new DirectoryInfo(fi.DirectoryName);
            foreach (var file in di.GetFiles())
            {
                file.Delete();
            }
            di.Delete();
        }
        public void Dispose()
        {
            Disconnect();
            Context?.Dispose();
        }

    }
}

