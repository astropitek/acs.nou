﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACS.NOU.ScheduleCardLib
{
    /// <summary>
    /// Представляет класс, содержащий идентификаторы типов карточек библиотеки "Расписания занятий"
    /// </summary>
    internal static class Cards
    {
        #region Константы - идентификаторы типов карточек библиотеки
        /// <summary>
        /// Идентификатор типа карточки "Создание расписания" (расширение Навигатора)
        /// </summary>
        public static readonly Guid Extension = new Guid("BCA17092-113D-46BB-A540-6BABC5202309");
        #endregion
    }
}
