﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using DocsVision.Platform.Extensibility;
using DocsVision.Platform.ObjectManager;
using DocsVision.Platform.ObjectManager.Metadata;
using DocsVision.Platform.ObjectManager.SearchModel;
using DocsVision.Platform.CardHost;
using DocsVision.Platform.WinForms;
using DocsVision.Platform.Data.Metadata;
using DocsVision.Platform.ObjectModel;
using DocsVision.Platform.ObjectModel.Mapping;
using DocsVision.Platform.ObjectModel.Persistence;
using DocsVision.Platform.ObjectManager.SystemCards;
using DocsVision.Platform.SystemCards.ObjectModel.Mapping;
using DocsVision.Platform.SystemCards.ObjectModel.Services;

using DocsVision.BackOffice.ObjectModel;
using DocsVision.BackOffice.ObjectModel.Mapping;
using DocsVision.BackOffice.ObjectModel.Services;

using System.ComponentModel.Design;

namespace Docsvision.Samples.NavigatorPlugin
{
    [ComVisible(true)]
    [Guid("106CAB08-48FA-43F0-AF70-9AB039D9DD47")]
    [ClassInterface(ClassInterfaceType.None)]
    public partial class Plugin : NavExtension
    {
        public Plugin() { }

        // Тип расширения
        protected override NavExtensionTypes SupportedTypes
        {
            get
            {
                return NavExtensionTypes.Command;
            }
        }

        // Название расширения
        protected override string GetExtensionName(NavExtensionTypes extensionType)
        {
            return "Расписание занятий";
        }

        // Инициализация списка команд
        protected override IEnumerable<NavCommand> CreateCommands()
        {
            NavCommand myCommand = new NavCommand();
            myCommand.CommandType = NavCommandTypes.ToolBar | NavCommandTypes.Picture; // В данном случае - команда размещается на ленте инструментов
            myCommand.Name = "Расписание занятий";
            myCommand.Description = "Создание расписаний для учебных групп";
            myCommand.Icon = Docsvision.Samples.NavigatorPlugin.Resources.CreateScheduleIcon;
            myCommand.Location = new NavCommandLocation()
            {
                RibbonTabLocations = new RibbonTabLocation[]
                { 
                 new RibbonTabLocation
                 {
                  Tab = "Доп. команды",
                  Groups = new string[] { "Группы" }
                 }
                }
            };
            return new List<NavCommand> { myCommand };
        }

        // Проверка команды на доступность 
        protected override NavCommandStatus QueryCommandStatus(NavCommand command, NavCommandContext context)
        {
            return NavCommandStatus.Supported | NavCommandStatus.Enabled; ; // всегда доступна
        }

        private ObjectContext CreateObjectContext(UserSession userSession)
        {
            var sessionContainer = new ServiceContainer();
            sessionContainer.AddService(typeof(UserSession), userSession);

            var objectContext = new ObjectContext(sessionContainer);

            var mapperFactoryRegistry = objectContext.GetService<IObjectMapperFactoryRegistry>();
            mapperFactoryRegistry.RegisterFactory(typeof(SystemCardsMapperFactory));
            mapperFactoryRegistry.RegisterFactory(typeof(BackOfficeMapperFactory));


            var serviceFactoryRegistry = objectContext.GetService<IServiceFactoryRegistry>();
            serviceFactoryRegistry.RegisterFactory(typeof(BackOfficeServiceFactory));
            serviceFactoryRegistry.RegisterFactory(typeof(SystemCardsServiceFactory));


            objectContext.AddService<IPersistentStore>(DocsVisionObjectFactory.CreatePersistentStore(new SessionProvider(userSession), null));

            IMetadataProvider metadataProvider = DocsVisionObjectFactory.CreateMetadataProvider(userSession);
            objectContext.AddService<IMetadataManager>(DocsVisionObjectFactory.CreateMetadataManager(metadataProvider, userSession));
            objectContext.AddService<IMetadataProvider>(metadataProvider);

            return objectContext;
        }

        protected void AddShortcutToFolder(Guid folderId, BaseCard card, ObjectContext context)
        {
            FolderCard folderCard = (FolderCard)Session.CardManager.GetDictionary(new Guid("DA86FABF-4DD7-4A86-B6FF-C58C24D12DE2"));
            folderCard.CreateShortcut(folderId, context.GetObjectRef(card).Id, false);
        }

        // Непосредственное исполнение команды расширения
        protected override void InvokeCommand(NavCommand command, NavCommandContext context)
        {
            //CardFrame.CardHost.ShowMessage("Информация","Программа в разработке","",DocsVision.Platform.CardHost.MessageType.Information,DocsVision.Platform.CardHost.MessageButtons.Ok, IntPtr.Zero);
            ObjectContext objectContext = CreateObjectContext(Session);
            IDocumentService documentService = objectContext.GetService<IDocumentService>();
            Document groupListCard = documentService.CreateDocument(null, objectContext.GetObject<KindsCardKind>(new Guid("FA9DD7DB-ED7F-482A-AA17-684B72400CC9")));
            groupListCard.MainInfo.Name = groupListCard.Description = "GroupList Test";
            objectContext.SaveObject(groupListCard);
            AddShortcutToFolder(new Guid("DCDBFE33-7BAE-4837-BF64-A5F2847BECB7"), groupListCard, objectContext);
            CardFrame.CardHost.ShowCardModal(objectContext.GetObjectRef(groupListCard).Id, ActivateMode.Edit);
            //Session.
            //CardFrame.
            /*
            string accountName = base.Session.Properties["AccountName"].Value.ToString();

            // Идентификаторы Справочника сотрудников и секции Сотрудники
            Guid RefStaff = new Guid("6710B92A-E148-4363-8A6F-1AA0EB18936C");
            Guid Employees = new Guid("DBC8AE9D-C1D2-4D5E-978B-339D22B32482");

            // Поиск в секции Сотрудники сотрудника с учетной записью accountName (учетная запись текущего сотрудника)
            SectionQuery sectionQuery = base.Session.CreateSectionQuery();
            sectionQuery.ConditionGroup.Conditions.AddNew("AccountName", FieldType.String, ConditionOperation.Equals, accountName);

            CardData staffData = base.Session.CardManager.GetDictionaryData(RefStaff, false);
            RowDataCollection users = staffData.Sections[Employees].FindRows(sectionQuery.GetXml());

            if (users.Count > 0)
            {

                // Получение Личной папки
                Guid? personalFolderID = users[0].GetGuid("PersonalFolder");
                if (personalFolderID.HasValue)
                {

                    // Установка курсора на папку
                    CardFrame.CardHost.ActivateFolder(personalFolderID.Value, true);
                }
            }
            */
        }
    }
}